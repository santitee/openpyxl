import openpyxl

wb = openpyxl.load_workbook('template.xlsx')

print(wb.sheetnames)
print("Active Sheet is " + wb.active.title)

sh = wb['Sheet1']
print(sh.title)

print(sh['A3'].value)
print(sh['B4'].value)

# for x in range(1, 101):
#     for y in range(1, 101):
#         sh.cell(row=x, column=y)
#         print(y)

# for row in sh.values:
#     for value in row:
#         print(value)

for row in sh.iter_rows(min_row=1, max_col=10, max_row=20, values_only=True):
    print(row)